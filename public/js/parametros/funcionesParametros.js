$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*-------------------------*/
    $(".inactivar_anio").on(tipoEvento, function() {
        var id = $(this).attr('id');
        inactivarAnio(id);
    });
    /*---------------------------*/
    $(".activar_anio").on(tipoEvento, function() {
        var id = $(this).attr('id');
        activarAnio(id)
    });
    /*-------------------------------------------------*/
    $(".valor_tasa").keyup(function() {
        var valor = $(this).val();
        var id = $(this).attr('id');
        valor = (valor / 100);
        var mensual = Math.pow((1 + valor), (30 / 360)) - 1;
        mensual = (mensual * 100);
        mensual = round(mensual);
        $(".valor_mensual" + id).val(mensual);
    });
    /*-------------------------------------------------*/
    function round(num, decimales = 2) {
        var signo = (num >= 0 ? 1 : -1);
        num = num * signo;
        if (decimales === 0) //con 0 decimales
            return signo * Math.round(num);
        // round(x * 10 ^ decimales)
        num = num.toString().split('e');
        num = Math.round(+(num[0] + 'e' + (num[1] ? (+num[1] + decimales) : decimales)));
        // x * 10 ^ (-decimales)
        num = num.toString().split('e');
        return signo * (num[0] + 'e' + (num[1] ? (+num[1] - decimales) : -decimales));
    }
    /*-------------------------------------------------*/
    function activarAnio(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/parametros/activar.php',
                method: 'POST',
                data: {
                    'id': id
                },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        $('.anio' + id).removeAttr('title');
                        $('.anio' + id + '').removeClass('btn btn-success btn-sm activar_anio').addClass('btn btn-danger btn-sm inactivar_anio');
                        $('.anio' + id + ' i').removeClass('fa-check').addClass('fa-times');
                        ohSnap("Activado correctamente!", {
                            color: "green",
                            'duration': '1000'
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("Error al activar!", {
                            color: "red",
                            'duration': '1000'
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*-------------------------------------------------*/
    function inactivarAnio(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/parametros/inactivar.php',
                method: 'POST',
                data: {
                    'id': id
                },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        $('.anio' + id).removeAttr('title');
                        $('.anio' + id + '').removeClass('btn btn-danger btn-sm inactivar_anio').addClass('btn btn-success btn-sm activar_anio');
                        $('.anio' + id + ' i').removeClass('fa-times').addClass('fa-check');
                        ohSnap("Activado correctamente!", {
                            color: "green",
                            'duration': '1000'
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("Error al activar!", {
                            color: "red",
                            'duration': '1000'
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina() {
        window.location.replace("index");
    }
});