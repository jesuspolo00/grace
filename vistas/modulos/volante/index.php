<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'torre' . DS . 'ControlTorre.php';

$instancia = ControlTorre::singleton_torre();

if (isset($_POST['buscar'])) {

} else {
	$datos_apartamentos = $instancia->mostrarDetalleApartamentosControl();
}

$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 10);
if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	die();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-haj">
						<a href="<?=BASE_URL?>inicio" class="text-decoration-none text-haj">
							<i class="fa fa-arrow-left"></i>
						</a>
						&nbsp;
						Cuentas de cobro
					</h4>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-8"></div>
							<div class="form-group col-lg-4">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" placeholder="Buscar" name="buscar" aria-describedby="basic-addon2">
									<div class="input-group-append">
										<button class="btn btn-haj btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<tr class="text-center font-weight-bold">
								<th scope="col">Torre</th>
								<th scope="col">Apartamento</th>
								<th scope="col">Responsable</th>
								<th scope="col">Mes a facturar</th>
							</tr>
							<tbody class="buscar">
								<?php
								foreach ($datos_apartamentos as $apartamento) {
									$id_apartamento = $apartamento['id'];
									$nom_apar       = $apartamento['nombre'];
									$nom_torre      = $apartamento['nom_torre'];
									$nom_usuario    = $apartamento['nom_usuario'];
									$activo         = $apartamento['activo'];

									$ver = ($activo == 0) ? 'd-none' : '';
									?>
									<tr class="text-center text-uppercase <?=$ver?>">
										<td><?=$nom_torre?></td>
										<td><?=$nom_apar?></td>
										<td><?=$nom_usuario?></td>
										<td><?=strftime("%B");?></td>
										<td>
											<div class="btn-group">
												<a href="<?=BASE_URL?>imprimir/cobro&codigo=<?=base64_encode($id_apartamento)?>" target="_blank" class="btn btn-haj btn-sm" data-tooltip="tooltip" title="Generar cuenta de cobro" data-trigger="hover" data-placement="bottom">
													<i class="fas fa-money-check"></i>
												</a>
											</div>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>