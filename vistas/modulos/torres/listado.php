<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'torre' . DS . 'ControlTorre.php';

$instancia = ControlTorre::singleton_torre();

$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 3);
if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	die();
}

if (isset($_GET['torre'])) {
	$id_torre = base64_decode($_GET['torre']);

	$datos_torre        = $instancia->mostrarDatosTorreControl($id_torre);
	$datos_apartamentos = $instancia->mostrarApartamentosControl($id_torre);
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-haj">
							<a href="<?=BASE_URL?>torres/index" class="text-decoration-none text-haj">
								<i class="fa fa-arrow-left"></i>
							</a>
							&nbsp;
							Listado de apartamentos - Torre (<?=$datos_torre['nombre']?>)
						</h4>
					</div>
					<div class="card-body">
						<form method="POST">
							<div class="row">
								<div class="col-lg-8"></div>
								<div class="form-group col-lg-4">
									<div class="input-group mb-3">
										<input type="text" class="form-control filtro" placeholder="Buscar" name="buscar" aria-describedby="basic-addon2">
										<div class="input-group-append">
											<button class="btn btn-haj btn-sm" type="submit">
												<i class="fa fa-search"></i>
												&nbsp;
												Buscar
											</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<div class="table-responsive mt-2">
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<tr class="text-center font-weight-bold">
									<th scope="col">Apartamento</th>
									<th scope="col">Responsable</th>
									<th scope="col">Telefono Responsable</th>
									<th scope="col">Correo Responsable</th>
								</tr>
								<tbody class="buscar">
									<?php
									foreach ($datos_apartamentos as $aparta) {
										$id_apar     = $aparta['id'];
										$nom_apar    = $aparta['nom_apar'];
										$id_user     = $aparta['id_user'];
										$nom_usuario = $aparta['nom_user'] . ' ' . $aparta['apellido'];
										$activo      = $aparta['activo'];
										$telefono    = $aparta['telefono'];
										$correo      = $aparta['correo'];
										?>
										<tr class="text-center">
											<td><?=$nom_apar?></td>
											<td><?=$nom_usuario?></td>
											<td><?=$telefono?></td>
											<td><?=$correo?></td>
											<td>
												<div class="btn-group">
													<button class="btn btn-haj btn-sm" data-tooltip="tooltip" title="Editar" data-placement="bottom" data-trigger="hover" disabled>
														<i class="fa fa-edit"></i>
													</button>
													<button class="btn btn-danger btn-sm" data-tooltip="tooltip" title="Eliminar" data-placement="bottom" data-trigger="hover" disabled>
														<i class="fa fa-times"></i>
													</button>
												</div>
											</td>
										</tr>
									<?php }?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';
}