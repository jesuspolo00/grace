<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'salon' . DS . 'ControlSalones.php';

$instancia = ControlSalon::singleton_salon();

$datos_salon           = $instancia->mostrarSalonesControl();
$datos_salones_reserva = $instancia->mostrarSalonesReservaControl();

$ver_botones = (isset($_POST['salon'])) ? '' : 'd-none';

if (isset($_POST['salon'])) {

	$buscar_salon = $_POST['salon'];
	$buscar_fecha = $_POST['fecha'];
	$buscar_user  = $_POST['buscar'];

	$datos = array('salon' => $buscar_salon, 'fecha' => $buscar_fecha, 'user' => $buscar_user, 'id' => $id_log);

	$datos_reserva = $instancia->buscarDatosDetalleReservaUsuarioControl($datos);
} else {
	$datos_reserva = $instancia->mostrarDatosDetalleReservaUsuarioControl($id_log);
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3">
					<h4 class="m-0 font-weight-bold text-haj">
						<a href="<?=BASE_URL?>inicio" class="text-decoration-none">
							<i class="fa fa-arrow-left text-haj"></i>
						</a>
						&nbsp;
						Mis reservas
					</h4>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
								<select name="salon" id="" class="form-control">
									<option value="" selected>Seleccionar una opcion...</option>
									<?php
									foreach ($datos_salon as $salon) {
										$id_salon  = $salon['id'];
										$salon_nom = $salon['nombre'];
										$activo    = $salon['activo'];

										$ver = ($activo == 0) ? 'd-none' : '';
										?>
										<option value="<?=$id_salon?>" class="<?=$ver?>"><?=$salon_nom?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<input type="date" class="form-control filtro_change" name="fecha">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group">
									<input type="text" class="form-control filtro" placeholder="Buscar" aria-describedby="basic-addon2" name="buscar">
									<div class="input-group-append">
										<button class="btn btn-haj btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>

					<div class="table-responsive mt-2">

						<?php
						foreach ($datos_salones_reserva as $salones) {
							$id_salon  = $salones['id'];
							$nom_salon = $salones['nombre'];
							?>
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center text-dark font-weight-bold text-uppercase">
										<th scope="col" colspan="5"><?=$nom_salon?></th>
									</tr>
									<tr class="text-center font-weight-bold">
										<th scope="col">Apartamento</th>
										<th scope="col">Usuario</th>
										<th scope="col">Fecha reserva</th>
										<th scope="col">Hora reserva</th>
										<th scope="col">Detalle reserva</th>
									</tr>
								</thead>
								<tbody>
									<?php
									foreach ($datos_reserva as $reserva) {
										$id_reserva         = $reserva['id_reserva'];
										$nom_salon          = $reserva['salon'];
										$fecha              = $reserva['fecha_reserva'];
										$hora               = $reserva['hora'];
										$usuario            = $reserva['nom_user'];
										$detalle            = $reserva['detalle_reserva'];
										$activo             = $reserva['detalle_activo'];
										$id_salon_re        = $reserva['id_salon'];
										$apartamento        = $reserva['nom_apa'];
										$id_user            = $reserva['id_user'];
										$id_detalle_reserva = $reserva['id_detalle_reserva'];

										if ($activo == 1 && $id_salon == $id_salon_re && $id_log == $id_user) {
											?>
											<tr class="text-center">
												<td><?=$apartamento?></td>
												<td><?=$usuario?></td>
												<td><?=$fecha?></td>
												<td><?=$hora?></td>
												<td><?=$detalle?></td>
												<td>
													<button class="btn btn-danger btn-sm" data-tooltip="tooltip" title="Cancelar reserva" data-placement="bottom" data-trigger="hover" data-toggle="modal" data-target="#cancelar_<?=$id_reserva?>">
														<i class="fas fa-ban"></i>
													</button>
												</td>
											</tr>

											<div class="modal fade" id="cancelar_<?=$id_reserva?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
												<div class="modal-dialog modal-lg" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<h5 class="modal-title text-haj font-weight-bold" id="exampleModalLabel">Cancelar Reserva</h5>
														</div>
														<div class="modal-body">
															<form method="POST">
																<input type="hidden" name="id_log" value="<?=$id_log?>">
																<input type="hidden" name="id_reserva" value="<?=$id_reserva?>">
																<input type="hidden" name="id_detalle_reserva" value="<?=$id_detalle_reserva?>">
																<div class="row p-2">
																	<div class="col-lg-6 form-group">
																		<label class="font-weight-bold">Salon</label>
																		<input type="text" class="form-control" value="<?=$nom_salon?>" disabled>
																	</div>
																	<div class="col-lg-6 form-group">
																		<label class="font-weight-bold">Apartamento</label>
																		<input type="text" class="form-control" value="<?=$apartamento?>" disabled>
																	</div>
																	<div class="col-lg-6 form-group">
																		<label class="font-weight-bold">Fecha reserva</label>
																		<input type="text" class="form-control" value="<?=$fecha?>" disabled>
																	</div>
																	<div class="col-lg-6 form-group">
																		<label class="font-weight-bold">Hora reserva</label>
																		<input type="text" class="form-control" value="<?=$hora?>" disabled>
																	</div>
																	<div class="col-lg-12 form-group">
																		<label class="font-weight-bold">Detalle reserva</label>
																		<textarea class="form-control" rows="5" disabled><?=$detalle?></textarea>
																	</div>
																	<div class="col-lg-12 form-group">
																		<label class="font-weight-bold">Motivo cancelacion (opcional)</label>
																		<textarea name="motivo" class="form-control" rows="5"></textarea>
																	</div>
																	<div class="col-lg-12 form-group text-right">
																		<button class="btn btn-danger btn-sm" data-dismiss="modal" type="button">
																			<i class="fas fa-sign-out-alt"></i>
																			&nbsp;
																			Salir
																		</button>
																		<button class="btn btn-haj btn-sm" type="submit">
																			<i class="fa fa-times"></i>
																			&nbsp;
																			Cancelar
																		</button>
																	</div>
																</div>
															</form>
														</div>
													</div>
												</div>
											</div>

										<?php }}?>
									</tbody>
								</table>
								<?php
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';

	if (isset($_POST['motivo'])) {
		$instancia->cancelarReservaControl();
	}
?>