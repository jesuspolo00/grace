<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'salon' . DS . 'ControlSalones.php';
require_once CONTROL_PATH . 'torre' . DS . 'ControlTorre.php';

$instancia       = ControlTorre::singleton_torre();
$instancia_salon = ControlSalon::singleton_salon();

$datos_apartamentos = $instancia->mostrarApartamentosUsuarioControl($id_log);
$datos_salon        = $instancia_salon->mostrarSalonesControl();

$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 5);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-haj">
                        <a href="<?=BASE_URL?>inicio" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-haj"></i>
                        </a>
                        &nbsp;
                        Reservas Sociales
                    </h4>
                    <a href="<?=BASE_URL?>salon/disponibilidad" class="btn btn-haj btn-sm">
                        <i class="fas fa-clock"></i>
                        &nbsp;
                        Consultar disponibilidad
                    </a>
                </div>
                <div class="card-body">
                    <form method="POST" id="form_apartar">
                        <input type="hidden" name="id_log" value="<?=$id_log?>">
                        <div class="row p-2">
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Apartamento <span class="text-danger">*</span></label>
                                <select class="form-control apartamento" required name="apartamento">
                                    <option value="" selected>Seleccione una opcion...</option>
                                    <?php
                                    foreach ($datos_apartamentos as $apar) {
                                        $id_apar   = $apar['id'];
                                        $nombre    = $apar['nombre'];
                                        $nom_torre = $apar['nom_torre'];
                                        ?>
                                        <option value="<?=$id_apar?>"><?=$nombre?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Salon <span class="text-danger">*</span></label>
                                <select name="salon" class="form-control salon" required>
                                    <option value="" selected="">Seleccione una opcion...</option>
                                    <?php
                                    foreach ($datos_salon as $salon) {
                                        $id_salon  = $salon['id'];
                                        $nom_salon = $salon['nombre'];
                                        $activo    = $salon['activo'];

                                        $ver = ($activo == 0) ? 'd-none' : '';

                                        ?>
                                        <option value="<?=$id_salon?>"><?=$nom_salon?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Fecha a apartar <span class="text-danger">*</span></label>
                                <input type="date" name="fecha_apartar" class="form-control fecha" required>
                            </div>
                            <div class="col-lg-12 form-group">
                                <label class="font-weight-bold">Horas disponibles <span class="text-danger">*</span></label>
                                <div class="hora form-inline"></div>
                            </div>
                            <div class="col-lg-12 form-group mt-1">
                                <label class="font-weight-bold">Detalle de reserva</label>
                                <textarea class="form-control" name="detalle_reserva" cols="5" rows="5"></textarea>
                            </div>
                            <div class="col-lg-12 form-group text-right mt-2">
                                <button class="btn btn-success btn-sm enviar_apartar" type="submit">
                                    <i class="fa fa-save"></i>
                                    &nbsp;
                                    Guardar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
    $instancia_salon->reservarSalonControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/salon/funcionesSalon.js"></script>