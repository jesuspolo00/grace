<!--Agregar usuario-->
<div class="modal fade" id="agregar_usuario" tabindex="-1" role="modal" aria-hidden="true" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg p-2" role="document">
        <div class="modal-content">
            <form method="POST">
                <input type="hidden" value="<?=$id_log?>" name="id_log">
                <div class="modal-header p-3">
                    <h4 class="modal-title text-haj font-weight-bold">Agregar Usuario</h4>
                </div>
                <div class="modal-body border-0">
                    <div class="row  p-3">
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Documento <span class="text-danger">*</span></label>
                            <input type="text" class="form-control numeros" name="documento" id="doc_user" required data-tooltip="tooltip" title="Presiona enter" data-placement="right" data-trigger="hover">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
                            <input type="text" class="form-control input" name="nombre" required disabled>
                        </div>
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Apellido <span class="text-danger">*</span></label>
                            <input type="text" class="form-control input" name="apellido" required disabled>
                        </div>
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Telefono</label>
                            <input type="text" class="form-control numeros input" name="telefono" disabled>
                        </div>
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Correo <span class="text-danger">*</span></label>
                            <input type="email" class="form-control input" name="correo" required disabled>
                        </div>
                        <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Perfil <span class="text-danger">*</span></label>
                            <select class="form-control input" name="perfil" required disabled>
                                <option selected value="">Seleccione una opcion...</option>
                                <?php
                                foreach ($datos_perfil as $perfiles) {
                                    $id_perfil  = $perfiles['id'];
                                    $nom_perfil = $perfiles['nombre'];

                                    if ($id_perfil != 1) {
                                        ?>
                                        <option value="<?=$id_perfil?>"><?=$nom_perfil?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-lg-12 form-group text-right">
                            <button class="btn btn-danger btn-sm" data-dismiss="modal">
                                <i class="fa fa-times"></i>
                                &nbsp;
                                Cancelar
                            </button>
                            <button type="submit" class="btn btn-haj btn-sm">
                                <i class="fa fa-save"></i>
                                &nbsp;
                                Registrar
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>