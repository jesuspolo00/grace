<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3">
					<h4 class="m-0 font-weight-bold text-haj">Modulos</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<?php
						$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 2);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>usuarios/index">
								<div class="card border-left-success shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Usuarios</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-users fa-2x text-success"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 3);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>torres/index">
								<div class="card border-left-info shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Torres</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-building fa-2x text-info"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 4);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>salon/index">
								<div class="card border-left-warning shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Salones</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-map-pin fa-2x text-warning"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 5);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>salon/apartar">
								<div class="card border-left-danger shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Reservas Sociales</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-calendar-alt fa-2x text-danger"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 6);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>salon/diario">
								<div class="card border-left-green shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Programacion diaria</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-clock fa-2x text-green"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 7);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>salon/reservas">
								<div class="card border-left-brown shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Mis reservas</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-calendar-day fa-2x text-brown"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 8);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>parametros/index">
								<div class="card border-left-green-force shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Valores</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-funnel-dollar fa-2x text-green-force"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 9);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>anios/index">
								<div class="card border-left-pink shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">A&ntilde;os</div>
											</div>
											<div class="col-auto">
												<i class="far fa-calendar fa-2x text-pink"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 10);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>volante/index">
								<div class="card border-left-purple shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Cuentas de cobro</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-money-check-alt fa-2x text-purple"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 1);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>permisos/index">
								<div class="card border-left-secondary shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Permisos</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-unlock-alt fa-2x text-secondary"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
