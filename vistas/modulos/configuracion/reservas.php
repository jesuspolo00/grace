<?php
require_once CONTROL_PATH . 'salon' . DS . 'ControlSalones.php';

$instancia = ControlSalon::singleton_salon();

$datos_salon           = $instancia->mostrarSalonesControl();
$datos_salones_reserva = $instancia->mostrarSalonesReservaControl();

$ver_botones = (isset($_POST['salon'])) ? '' : 'd-none';

if (isset($_POST['salon'])) {

	$buscar_salon = $_POST['salon'];
	$buscar_fecha = $_POST['fecha'];
	$buscar_user  = $_POST['buscar'];

	$datos = array('salon' => $buscar_salon, 'fecha' => $buscar_fecha, 'user' => $buscar_user);

	$datos_reserva = $instancia->buscarDatosDetalleReservaControl($datos);
} else {
	$datos_reserva = $instancia->mostrarDatosDetalleReservaControl();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3">
					<h4 class="m-0 font-weight-bold text-haj">Mis reservas</h4>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
								<select name="salon" id="" class="form-control">
									<option value="" selected>Seleccionar una opcion...</option>
									<?php
									foreach ($datos_salon as $salon) {
										$id_salon  = $salon['id'];
										$salon_nom = $salon['nombre'];
										$activo    = $salon['activo'];

										$ver = ($activo == 0) ? 'd-none' : '';
										?>
										<option value="<?=$id_salon?>" class="<?=$ver?>"><?=$salon_nom?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<input type="date" class="form-control filtro_change" name="fecha">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group">
									<input type="text" class="form-control filtro" placeholder="Buscar" aria-describedby="basic-addon2" name="buscar">
									<div class="input-group-append">
										<button class="btn btn-haj btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>

					<div class="table-responsive mt-2">

						<?php
						foreach ($datos_salones_reserva as $salones) {
							$id_salon  = $salones['id'];
							$nom_salon = $salones['nombre'];
							?>
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center text-dark font-weight-bold text-uppercase">
										<th scope="col" colspan="5"><?=$nom_salon?></th>
									</tr>
									<tr class="text-center font-weight-bold">
										<th scope="col">Apartamento</th>
										<th scope="col">Usuario</th>
										<th scope="col">Fecha reserva</th>
										<th scope="col">Hora reserva</th>
										<th scope="col">Detalle reserva</th>
									</tr>
								</thead>
								<tbody>
									<?php
									foreach ($datos_reserva as $reserva) {
										$id_reserva  = $reserva['id_reserva'];
										$nom_salon   = $reserva['salon'];
										$fecha       = $reserva['fecha_reserva'];
										$hora        = $reserva['hora'];
										$usuario     = $reserva['nom_user'];
										$detalle     = $reserva['detalle_reserva'];
										$activo      = $reserva['activo'];
										$id_salon_re = $reserva['id_salon'];
										$apartamento = $reserva['nom_apa'];
										$id_user     = $reserva['id_user'];

										if ($activo == 1 && $id_salon == $id_salon_re && $id_log == $id_user) {
											?>
											<tr class="text-center">
												<td><?=$apartamento?></td>
												<td><?=$usuario?></td>
												<td><?=$fecha?></td>
												<td><?=$hora?></td>
												<td><?=$detalle?></td>
											</tr>

										<?php }}?>
									</tbody>
								</table>
								<?php
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
