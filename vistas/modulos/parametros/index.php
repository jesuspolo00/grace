<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'parametros' . DS . 'ControlParametros.php';

$instancia = ControlParametros::singleton_parametros();

$datos_anios      = $instancia->mostrarAniosControl();
$datos_empresa    = $instancia->mostrarDatosEmpresaControl();
$datos_parametros = $instancia->mostrarDatosParametrosControl();

$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 8);
if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	die();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-haj">
						<a href="<?=BASE_URL?>inicio" class="text-decoration-none text-haj">
							<i class="fa fa-arrow-left"></i>
						</a>
						&nbsp;
						Valores parametrizables
					</h4>
				</div>
				<div class="card-body">
					<form method="POST">
						<input type="hidden" value="<?=$id_log?>" name="id_log">
						<input type="hidden" value="<?=$datos_empresa['id']?>" name="id_empresa">
						<div class="row p-2">
							<div class="col-lg-12 form-group text-center mb-4">
								<h5 class="font-weight-bold text-haj text-uppercase">Informacion del conjunto</h5>
								<hr>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Nombre del conjunto <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="nombre" required value="<?=$datos_empresa['nombre']?>">
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">NIT <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="nit" required value="<?=$datos_empresa['nit']?>">
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Telefono</label>
								<input type="text" class="form-control" name="telefono" value="<?=$datos_empresa['telefono']?>">
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Direccion</label>
								<input type="text" class="form-control" name="direccion" value="<?=$datos_empresa['direccion']?>">
							</div>
							<div class="col-lg-12 form-group text-right mt-2">
								<button class="btn btn-haj btn-sm" type="submit">
									<i class="fa fa-sync-alt"></i>
									&nbsp;
									Actualizar informacion
								</button>
							</div>
						</div>
					</form>
				</div>
				<div class="table-responsive mt-2 p-2">
					<table class="table table-hover border table-sm" width="100%" cellspacing="0">
						<tr class="text-center font-weight-bold text-uppercase">
							<th scope="col" colspan="5">Valores parametrizados Anuales</th>
							<th>
								<button class="btn btn-sm btn-haj" data-tooltip="tooltip" title="Agregar valores" data-trigger="hover" data-placement="bottom" data-toggle="modal" data-target="#agregar_valores">
									<i class="fa fa-plus"></i>
								</button>
							</th>
						</tr>
						<tr class="text-center">
							<th scope="col">Valor Administracion</th>
							<th scope="col">Valor Tasa efectiva anual</th>
							<th scope="col">Dia mes inicio pago</th>
							<th scope="col">Dia mes fin pago</th>
							<th scope="col">A&ntilde;o</th>
						</tr>
						<tbody class="buscar">
							<?php
							foreach ($datos_parametros as $parametros) {
								$id_parametros = $parametros['id'];
								$valor_admin   = $parametros['valor_admin'];
								$dia_inicio    = $parametros['dia_inicio'];
								$dia_fin       = $parametros['dia_fin'];
								$anio          = $parametros['anio'];
								$valor_tasa    = $parametros['valor_tasa'];
								?>
								<tr class="text-center">
									<td>$<?=number_format($valor_admin)?></td>
									<td><?=$valor_tasa?>%</td>
									<td><?=$dia_inicio?></td>
									<td><?=$dia_fin?></td>
									<td><?=$anio?></td>
									<td>
										<div class="btn-group">
											<a href="<?=BASE_URL?>parametros/detalle?anio=<?=base64_encode($parametros['id_anio'])?>" class="btn btn-info btn-sm" data-tooltip="tooltip" title="Detalles del a&ntilde;o">
												<i class="fa fa-eye"></i>
											</a>
											<button class="btn btn-haj btn-sm" data-tooltip="tooltip" title="Editar">
												<i class="fa fa-edit"></i>
											</button>
										</div>
									</td>
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'parametros' . DS . 'agregarValores.php';

if (isset($_POST['nombre'])) {
	$instancia->actualizarInformacionEmpresaControl();
}

if (isset($_POST['valor_admin'])) {
	$instancia->agregarValoresControl();
}
?>