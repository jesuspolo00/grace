<div class="modal fade" id="agregar_valores" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-haj font-weight-bold" id="exampleModalLabel">Agregar Valores</h5>
      </div>
      <div class="modal-body">
        <form method="POST">
          <input type="hidden" name="id_log" value="<?=$id_log?>">
          <div class="row p-2">
            <div class="form-group col-lg-6">
              <label class="font-weight-bold">Valor de la administracion <span class="text-danger">*</span></label>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">$</span>
                </div>
                <input type="text" class="form-control valores" name="valor_admin" aria-describedby="basic-addon1">
              </div>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">A&ntilde;o <span class="text-danger">*</span></label>
              <select class="form-control" name="anio" required>
                <option value="" selected>Seleccione una opcion...</option>
                <?php
                foreach ($datos_anios as $anios) {
                  $id_anio = $anios['id'];
                  $anio    = $anios['anio'];
                  $activo  = $anios['activo'];

                  $texto = ($activo == 1) ? 'Activo' : 'Inactivo';
                  ?>
                  <option value="<?=$id_anio?>"><?=$anio . ' (' . $texto . ')'?></option>
                  <?php
                }
                ?>
              </select>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Dia inicio pago <span class="text-danger">*</span></label>
              <input type="text" class="form-control numeros" name="dia_inicio" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Dia fin pago <span class="text-danger">*</span></label>
              <input type="text" class="form-control numeros" name="dia_fin" required>
            </div>
          </div>
          <div class="col-lg-12 form-group text-right mt-2">
            <button class="btn btn-danger btn-sm"  type="button" data-dismiss="modal">
              <i class="fa fa-times"></i>
              &nbsp;
              Cerrar
            </button>
            <button class="btn btn-haj btn-sm" type="submit">
              <i class="fa fa-save"></i>
              &nbsp;
              Guardar
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
