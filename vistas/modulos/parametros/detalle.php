<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'parametros' . DS . 'ControlParametros.php';

$instancia = ControlParametros::singleton_parametros();

$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 8);
if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	die();
}

if (isset($_GET['anio'])) {
	$id_anio = base64_decode($_GET['anio']);

	$datos_anio  = $instancia->mostrarDatosAnioControl($id_anio);
	$datos_meses = $instancia->mostrarDatosMesesAnioControl($id_anio);
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-haj">
							<a href="<?=BASE_URL?>parametros/index" class="text-decoration-none text-haj">
								<i class="fa fa-arrow-left"></i>
							</a>
							&nbsp;
							Detalles del año (<?=$datos_anio['anio']?>)
						</h4>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-8"></div>
							<div class="form-group col-lg-4">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" placeholder="Buscar" name="buscar" aria-describedby="basic-addon2">
									<div class="input-group-append">
										<button class="btn btn-haj btn-sm" type="button">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
						<div class="table-responsive mt-2">
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<tr class="text-center font-weight-bold">
									<th scope="col">A&ntilde;o</th>
									<th scope="col">Mes</th>
									<th scope="col">Valor Tasa Efectiva (T.E.A)</th>
									<th scope="col">Valor Tasa Efectiva Mensual</th>
									<th scope="col">Valor Administracion</th>
								</tr>
								<tbody class="buscar">
									<?php
									foreach ($datos_meses as $mes) {
										$id_mes_anio = $mes['id'];
										$nom_mes     = $mes['nom_mes'];
										$nom_anio    = $mes['anio'];
										$valor_anual = $mes['valor_tasa_anual'];
										$admin       = $mes['valor_admin'];

										$tasaefectivaanual = ($valor_anual / 100);
										$valor_mensual     = pow((1 + $tasaefectivaanual), (30 / 360)) - 1;
										$valor_mensual     = ($valor_mensual * 100);
										?>
										<tr class="text-center text-uppercase">
											<td><?=$nom_anio?></td>
											<td><?=$nom_mes?></td>
											<td><?=$valor_anual?>%</td>
											<td><?=round($valor_mensual, 2)?>%</td>
											<td>$<?=number_format($admin)?></td>
											<td>
												<button class="btn btn-haj btn-sm" data-tooltip="tooltip" title="Editar" data-trigger="hover" data-placement="bottom" data-toggle="modal" data-target="#editar<?=$id_mes_anio?>">
													<i class="fa fa-edit"></i>
												</button>
											</td>
										</tr>


										<div class="modal fade" id="editar<?=$id_mes_anio?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title text-haj font-weight-bold" id="exampleModalLabel">Editar Valores del mes (<?=$nom_mes?> - <?=$nom_anio?>)</h5>
													</div>
													<div class="modal-body">
														<form method="POST">
															<input type="hidden" name="id_log" value="<?=$id_log?>">
															<input type="hidden" name="id_mes_anio" value="<?=$id_mes_anio?>">
															<input type="hidden" name="id_anio" value="<?=$id_anio?>">
															<div class="row p-1">
																<div class="col-lg-6 form-group">
																	<label class="font-weight-bold">Valor tasa efectiva anual (T.E.A) <span class="text-danger">*</span></label>
																	<div class="input-group mb-3">
																		<div class="input-group-prepend">
																			<span class="input-group-text" id="basic-addon1">%</span>
																		</div>
																		<input type="text" class="form-control valor_tasa" id="<?=$id_mes_anio?>" name="valor_tasa_edit" aria-describedby="basic-addon1" value="<?=$valor_anual?>" required>
																	</div>
																</div>
																<div class="col-lg-6 form-group">
																	<label class="font-weight-bold">Valor tasa efectiva MENSUAL <span class="text-danger">*</span></label>
																	<div class="input-group mb-3">
																		<div class="input-group-prepend">
																			<span class="input-group-text" id="basic-addon1">%</span>
																		</div>
																		<input type="text" class="form-control valor_mensual<?=$id_mes_anio?>" disabled aria-describedby="basic-addon1" value="<?=round($valor_mensual, 2)?>">
																	</div>
																</div>
																<div class="form-group col-lg-12 text-right">
																	<button class="btn btn-danger btn-sm" data-dismiss="modal" type="button">
																		<i class="fa fa-times"></i>
																		&nbsp;
																		Cancelar
																	</button>
																	<button class="btn btn-haj btn-sm"  type="submit">
																		<i class="fa fa-save"></i>
																		&nbsp;
																		Guardar
																	</button>
																</div>
															</div>
														</form>
													</div>
												</div>
											</div>
										</div>
										<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';

	if (isset($_POST['id_mes_anio'])) {
		$instancia->actualizarValoresMesAnioControl();
	}
}
?>
<script src="<?=PUBLIC_PATH?>js/parametros/funcionesParametros.js"></script>