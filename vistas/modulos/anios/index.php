<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'parametros' . DS . 'ControlParametros.php';

$instancia = ControlParametros::singleton_parametros();

$datos_anios = $instancia->mostrarAniosControl();

$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 9);
if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	die();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-haj">
						<a href="<?=BASE_URL?>inicio" class="text-decoration-none text-haj">
							<i class="fa fa-arrow-left"></i>
						</a>
						&nbsp;
						A&ntilde;os
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
							<div class="dropdown-header">Acciones:</div>
							<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_anio">Agregar A&ntilde;o</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-8"></div>
							<div class="form-group col-lg-4">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" placeholder="Buscar" name="buscar" aria-describedby="basic-addon2">
									<div class="input-group-append">
										<button class="btn btn-haj btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<tr class="text-center font-weight-bold">
								<th scope="col">A&ntilde;o</th>
								<th scope="col">Valor Tasa Efectiva Anual (T.E.A)</th>
								<th scope="col">Estado</th>
							</tr>
							<tbody class="buscar">
								<?php
								foreach ($datos_anios as $anios) {
									$id_anio    = $anios['id'];
									$nom_anio   = $anios['anio'];
									$activo     = $anios['activo'];
									$valor_tasa = $anios['valor_tasa'];

									$span = ($activo == 0) ? '<span class="badge badge-danger anio_span' . $id_anio . '">Inactivo</span>' : '<span class="badge badge-success anio_span' . $id_anio . '">Activo</span>';

									$class = ($activo == 1) ? 'btn-danger inactivar_anio' : 'btn-success activar_anio';
									$title = ($activo == 1) ? 'Inactivar' : 'Activar';
									$icon  = ($activo == 1) ? '<i class="fa fa-times"></i>' : '<i class="fa fa-check"></i>';

									?>
									<tr class="text-center">
										<td><?=$nom_anio?></td>
										<td><?=$valor_tasa?>%</td>
										<td><?=$span?></td>
										<td>
											<div class="btn-group">
												<button class="btn btn-sm <?=$class?> anio<?=$id_anio?>" data-tooltip="tooltip" title="<?=$title?>" data-placement="bottom" data-trigger="hover" id="<?=$id_anio?>"  type="button">
													<?=$icon?>
												</button>
											</div>
										</td>
									</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'anios' . DS . 'agregarAnio.php';

if (isset($_POST['anio'])) {
	$instancia->agregarAnioControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/parametros/funcionesParametros.js"></script>