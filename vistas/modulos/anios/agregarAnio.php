<!--Agregar usuario-->
<div class="modal fade" id="agregar_anio" tabindex="-1" role="modal" aria-hidden="true" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog p-2" role="document">
        <div class="modal-content">
            <form method="POST">
                <input type="hidden" value="<?=$id_log?>" name="id_log">
                <div class="modal-header p-3">
                    <h4 class="modal-title text-haj font-weight-bold">Agregar A&ntilde;o</h4>
                </div>
                <div class="modal-body border-0">
                    <div class="row p-1">
                        <div class="col-lg-12 form-group">
                            <label class="font-weight-bold">A&ntilde;o <span class="text-danger">*</span></label>
                            <input type="text" class="form-control numeros" name="anio" required>
                        </div>
                        <div class="form-group col-lg-12">
                          <label class="font-weight-bold">Valor tasa efectiva anual <span class="text-danger">*</span></label>
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="basic-addon1">%</span>
                          </div>
                          <input type="text" class="form-control" name="valor_tasa" aria-describedby="basic-addon1">
                      </div>
                  </div>
                  <div class="col-lg-12 form-group text-right mt-2">
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">
                        <i class="fa fa-times"></i>
                        &nbsp;
                        Cancelar
                    </button>
                    <button type="submit" class="btn btn-haj btn-sm">
                        <i class="fa fa-save"></i>
                        &nbsp;
                        Registrar
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
</div>
</div>