<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'torre' . DS . 'ControlTorre.php';
require_once CONTROL_PATH . 'parametros' . DS . 'ControlParametros.php';

$instancia            = ControlTorre::singleton_torre();
$instancia_parametros = ControlParametros::singleton_parametros();

if (isset($_GET['codigo'])) {

    $id_apartamento = base64_decode($_GET['codigo']);

    $datos_apartamento = $instancia->mostrarDatosApartamentoControl($id_apartamento);
    $datos_parametros  = $instancia_parametros->mostrarDatosParametrosAnioControl(date('Y'));

    class MYPDF extends TCPDF
    {
        public function setData($logo)
        {
            $this->logo = $logo;
        }

        public function Header()
        {
        }

        public function Footer()
        {
            $this->SetY(-15);
            $this->SetFillColor(127);
            $this->SetTextColor(127);
            $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
            $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
        }
    }

    $pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('G.R.A.C.E');
    $pdf->SetTitle('Cuenta_cobro');
    $pdf->SetSubject('Cuenta_cobro');
    $pdf->SetKeywords('Cuenta_cobro');
    $pdf->setPageOrientation('L');
    $pdf->AddPage();

    $ln = 10;

    $pdf->Ln(1);
    $pdf->Cell(8);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 15);
    $pdf->MultiCell(120, 5, 'CONJUNTO RESIDENCIAL PORTAL LOS MANANTIALES MANZANA 3A', 0, 'C', 0, 0, '', '', true);
    $pdf->MultiCell(170, 5, 'CUENTA DE COBRO No. 3556', 0, 'C', 0, 0, '', '', true);

    $pdf->Ln(14);
    $pdf->Cell(8);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 15);
    $pdf->MultiCell(120, 5, 'Nit: 901170807', 0, 'C', 0, 0, '', '', true);

    $pdf->Ln(-0.5);
    $pdf->Cell(170);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 14);
    $pdf->MultiCell(80, 5, 'Torre: ' . $datos_apartamento['nom_torre'], 0, 'L', 0, 0, '', '', true);
    $pdf->Ln(6);
    $pdf->Cell(170);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 14);
    $pdf->MultiCell(80, 5, 'Apartamento: ' . $datos_apartamento['nom_apar'], 0, 'L', 0, 0, '', '', true);

    $pdf->Ln($ln + 4);
    $pdf->Cell(8);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 13);
    $pdf->MultiCell(140, 5, 'Cliente: ' . $datos_apartamento['usuario_asignado'], 1, 'L', 0, 0, '', '', true);
    $pdf->MultiCell(125, 5, 'Por concepto de:', 1, 'L', 0, 0, '', '', true);

    $pdf->Ln($ln + 2);
    $pdf->Cell(8);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 13);
    $pdf->MultiCell(140, 5, 'Identificacion: ' . $datos_apartamento['nom_torre'] . '-' . $datos_apartamento['nom_apar'], 1, 'L', 0, 0, '', '', true);
    $pdf->MultiCell(125, 20, '', 1, 'L', 0, 0, '', '', true);

    $pdf->Ln(7);
    $pdf->Cell(8);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 13);
    $pdf->MultiCell(46.6, 5, 'Fecha Factura:', 1, 'L', 0, 0, '', '', true);
    $pdf->MultiCell(46.6, 5, 'Fecha Vencimiento:', 1, 'L', 0, 0, '', '', true);
    $pdf->MultiCell(46.6, 5, 'Forma de pago:', 1, 'L', 0, 0, '', '', true);

    $pdf->Ln(7);
    $pdf->Cell(8);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 13);
    $pdf->MultiCell(46.6, 5, '01-nov-21', 1, 'C', 0, 0, '', '', true);
    $pdf->MultiCell(46.6, 5, '15-nov-21:', 1, 'C', 0, 0, '', '', true);
    $pdf->MultiCell(46.6, 5, 'Credito', 1, 'C', 0, 0, '', '', true);

    $pdf->Ln(8);
    $pdf->Cell(8);
    $tabla = '
    <table border="1" cellpadding="3" style="font-size:1.1em; width:98.5%;">
    <thead>
    <tr style="text-align:center; font-weight:bold;">
    <th scope="col" style="width:30%;">Descripcion</th>
    <th scope="col" style="width:10%;">Cantidad</th>
    <th scope="col" style="width:10%;">U Medida</th>
    <th scope="col" style="width:20%;">Valor Unitario</th>
    <th scope="col" style="width:10%;">Iva</th>
    <th scope="col" style="width:20%;">Total</th>
    </tr>
    </thead>
    <tbody>
    <tr style="text-align:center;">
    <td style="width:30%;">Cuota de administracion</td>
    <td style="width:10%;">1</td>
    <td style="width:10%;">Und</td>
    <td style="width:20%;">50.000</td>
    <td style="width:10%;">0%</td>
    <td style="width:20%;">50.000</td>
    </tr>
    <tr style="text-align:center;">
    <td style="width:30%;"></td>
    <td style="width:10%;"></td>
    <td style="width:10%;"></td>
    <td style="width:20%;"></td>
    <td style="width:10%;"></td>
    <td style="width:20%;"></td>
    </tr>
    <tr style="text-align:center;">
    <td style="width:30%;"></td>
    <td style="width:10%;"></td>
    <td style="width:10%;"></td>
    <td style="width:20%;"></td>
    <td style="width:10%;"></td>
    <td style="width:20%;"></td>
    </tr>
    </tbody>
    </table>
    ';

    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->writeHTML($tabla, true, false, true, false, '');

    $pdf->Ln(-5.8);
    $pdf->Cell(140.5);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 13);
    $pdf->MultiCell(80, 5, 'Total Mes Actual:', 1, 'L', 0, 0, '', '', true);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 13);
    $pdf->MultiCell(52.2, 5, '$50.000', 1, 'R', 0, 0, '', '', true);

    $pdf->Ln(6);
    $pdf->Cell(140.5);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 13);
    $pdf->MultiCell(132.2, 5, 'SALDOS ANTERIORES    ', 1, 'C', 0, 0, '', '', true);

    $pdf->Ln(6);
    $pdf->Cell(140.5);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 12);
    $pdf->MultiCell(80, 6, 'Cuota de administracion:', 1, 'L', 0, 0, '', '', true);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 12);
    $pdf->MultiCell(52.2, 6, '$50.000', 1, 'R', 0, 0, '', '', true);

    $pdf->Ln(6);
    $pdf->Cell(140.5);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 12);
    $pdf->MultiCell(80, 6, 'Intereses de mora:', 1, 'L', 0, 0, '', '', true);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 12);
    $pdf->MultiCell(52.2, 6, '$12', 1, 'R', 0, 0, '', '', true);

    $pdf->Ln(6);
    $pdf->Cell(140.5);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 12);
    $pdf->MultiCell(80, 6, 'Sancion Disciplinaria:', 1, 'L', 0, 0, '', '', true);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 12);
    $pdf->MultiCell(52.2, 6, '$0', 1, 'R', 0, 0, '', '', true);

    $pdf->Ln(6);
    $pdf->Cell(140.5);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 13);
    $pdf->MultiCell(80, 5, 'TOTAL SALDO ANTERIOR:', 1, 'L', 0, 0, '', '', true);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 13);
    $pdf->MultiCell(52.2, 5, '$50.012', 1, 'R', 0, 0, '', '', true);

    $pdf->Ln(6);
    $pdf->Cell(140.5);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 13);
    $pdf->MultiCell(80, 5, 'TOTAL ESTADO CUENTA:', 1, 'L', 0, 0, '', '', true);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 13);
    $pdf->MultiCell(52.2, 5, '$100.012', 1, 'R', 0, 0, '', '', true);

    $nombre_archivo = 'Cuenta_cobro' . date('Y_m_d');
//$carpeta_destino = PUBLIC_PATH_ARCH . 'upload/' . $nombre_archivo;

    $pdf->Output($nombre_archivo . '.pdf', 'I');
//$pdf->Output($carpeta_destino . '.pdf', 'F');

}
