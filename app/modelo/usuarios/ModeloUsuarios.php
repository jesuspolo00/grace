<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloUsuarios extends conexion
{

    public static function mostrarUsuariosModel()
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        p.nombre AS nom_perfil
        FROM " . $tabla . " u
        LEFT JOIN perfiles p ON p.id = u.perfil WHERE u.perfil NOT IN(1) ORDER BY u.nombre ASC LIMIT 50;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosUsuariosModel($id)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        p.nombre AS nom_perfil
        FROM " . $tabla . " u
        LEFT JOIN perfiles p ON p.id = u.perfil WHERE u.id_user = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarUsuariosBuscarControl($buscar)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        p.nombre AS nom_perfil
        FROM " . $tabla . " u
        LEFT JOIN perfiles p ON p.id = u.perfil
        WHERE CONCAT(u.nombre, ' ' , u.apellido, ' ', u.correo,  ' ', u.telefono, ' ', u.user) LIKE '%" . $buscar . "%' AND u.perfil NOT IN(1) ORDER BY u.nombre ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function agregarUsuarioModel($datos)
    {
        $tabla = 'usuarios';
        $cnx   = conexion::singleton_conexion();
        $sql   = "INSERT INTO " . $tabla . " (documento, nombre, apellido, correo, telefono, perfil, user, pass, user_log) VALUES (:d, :n, :a, :c, :t, :p, :us, :pass, :idl);";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':d', $datos['documento']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':a', $datos['apellido']);
            $preparado->bindParam(':c', $datos['correo']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':p', $datos['perfil']);
            $preparado->bindParam(':us', $datos['user']);
            $preparado->bindParam(':pass', $datos['pass']);
            $preparado->bindParam(':idl', $datos['id_log']);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('id' => $id, 'guardar' => true);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarUsuarioModel($datos)
    {
        $tabla = 'usuarios';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET documento = :d, nombre = :n, apellido = :a, correo = :c, telefono = :t, perfil = :r, user_log = :idl WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindValue(':id', $datos['id_user']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':d', $datos['documento']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':a', $datos['apellido']);
            $preparado->bindParam(':c', $datos['correo']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':r', $datos['perfil']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function inactivarUsuarioModel($id)
    {
        $tabla = 'usuarios';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET activo = 0, fecha_inactivo = NOW() WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function activarUsuarioModel($id)
    {
        $tabla = 'usuarios';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET activo = 1, fecha_activo = NOW() WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function restablecerUsuarioModel($datos)
    {
        $tabla = 'usuarios';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET pass = :p WHERE id_user = :id;";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':p', $datos['pass']);
            $preparado->bindParam(':id', $datos['id_user']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function consultarDocumentoModel($id)
    {
        $tabla = 'usuarios';
        $cnx   = conexion::singleton_conexion();
        $sql   = "SELECT * FROM " . $tabla . " WHERE documento = '" . $id . "' ORDER BY id_user DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($sql);
            //$preparado->bindParam(':d', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function comandoSQL()
    {
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SET SQL_BIG_SELECTS=1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
