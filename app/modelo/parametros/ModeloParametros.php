<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloParametros extends conexion
{

    public static function actualizarInformacionEmpresaModel($datos)
    {
        $tabla  = 'empresa';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET nombre = :n, nit = :nit, telefono = :t, direccion = :d, id_log = :idl WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':nit', $datos['nit']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':d', $datos['direccion']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':id', $datos['id_empresa']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosEmpresaModel()
    {
        $tabla  = 'empresa';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE * FROM " . $tabla;
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarAniosModel()
    {
        $tabla  = 'anio_activo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE * FROM " . $tabla . " ORDER BY activo DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function agregarValoresModel($datos)
    {
        $tabla  = 'parametros';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (valor_admin, dia_inicio, dia_fin, id_anio, id_log) VALUES (:v,:di,:df,:ida,:idl);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':v', $datos['valor_admin']);
            $preparado->bindParam(':di', $datos['dia_inicio']);
            $preparado->bindParam(':df', $datos['dia_fin']);
            $preparado->bindParam(':ida', $datos['anio']);
            $preparado->bindParam(':idl', $datos['id_log']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function agregarMesesAnioModel($datos)
    {
        $tabla  = 'meses_anio';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_anio, mes, valor_tasa_anual, id_log) VALUES (:ida, :m, :vt, :idl);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ida', $datos['anio']);
            $preparado->bindParam(':m', $datos['mes']);
            $preparado->bindParam(':vt', $datos['valor_tasa']);
            $preparado->bindParam(':idl', $datos['id_log']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function agregarAnioModel($datos)
    {
        $tabla  = 'anio_activo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (anio, valor_tasa, id_log) VALUES (:a, :vt, :idl)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':a', $datos['anio']);
            $preparado->bindParam(':vt', $datos['valor_tasa']);
            $preparado->bindParam(':idl', $datos['id_log']);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('id' => $id, 'guardar' => true);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosParametrosModel()
    {
        $tabla  = 'parametros';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "
        SELECT
        p.*,
        a.anio,
        a.valor_tasa
        FROM " . $tabla . " p
        LEFT JOIN anio_activo a ON a.id = p.id_anio;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosParametrosAnioModel($anio)
    {
        $tabla  = 'parametros';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "
        SELECT
        p.*,
        a.anio,
        a.valor_tasa
        FROM " . $tabla . " p
        LEFT JOIN anio_activo a ON a.id = p.id_anio WHERE a.anio = '" . $anio . "' ORDER BY p.id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosAnioModel($id)
    {
        $tabla  = 'anio_activo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE * FROM " . $tabla . " WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosMesesAnioModel($id)
    {
        $tabla  = 'anio_activo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ma.*,
        m.nombre AS nom_mes,
        a.anio,
        p.valor_admin
        FROM meses_anio ma
        LEFT JOIN anio_activo a ON a.id = ma.id_anio
        LEFT JOIN meses m ON m.id = ma.mes
        LEFT JOIN parametros p ON p.id_anio = a.id
        WHERE ma.id_anio = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function activarAnioModel($id)
    {
        $tabla = 'anio_activo';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET activo = 1 WHERE id = :id";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function inactivarAnioModel($id)
    {
        $tabla = 'anio_activo';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET activo = 0 WHERE id = :id";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarValoresMesAnioModel($datos)
    {
        $tabla = 'meses_anio';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET valor_tasa_anual = :vt, id_log = :idl, fecha_actualizado = '" . date('Y-m-d H:i:s') . "' WHERE id = :id";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':vt', $datos['valor']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':id', $datos['id_mes_anio']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function comandoSQL()
    {
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SET SQL_BIG_SELECTS=1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
