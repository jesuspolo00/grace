<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'salon' . DS . 'ModeloSalon.php';
require_once MODELO_PATH . 'usuarios' . DS . 'ModeloUsuarios.php';
require_once MODELO_PATH . 'torre' . DS . 'ModeloTorre.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreos.php';

class ControlSalon
{

    private static $instancia;

    public static function singleton_salon()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarSalonesControl()
    {
        $comando = ModeloSalon::comandoSQL();
        $mostrar = ModeloSalon::mostrarSalonesModel();
        return $mostrar;
    }

    public function mostrarDatosSalonesControl($id)
    {
        $comando = ModeloSalon::comandoSQL();
        $mostrar = ModeloSalon::mostrarDatosSalonesModel($id);
        return $mostrar;
    }

    public function mostrarSalonesReservaControl()
    {
        $comando = ModeloSalon::comandoSQL();
        $mostrar = ModeloSalon::mostrarSalonesReservaModel();
        return $mostrar;
    }

    public function mostrarHorasControl()
    {
        $comando = ModeloSalon::comandoSQL();
        $mostrar = ModeloSalon::mostrarHorasModel();
        return $mostrar;
    }

    public function mostrarHorasSalonControl($id)
    {
        $comando = ModeloSalon::comandoSQL();
        $mostrar = ModeloSalon::mostrarHorasSalonModel($id);
        return $mostrar;
    }

    public function buscarHoraPicoSalonControl($hora, $salon)
    {
        $comando = ModeloSalon::comandoSQL();
        $mostrar = ModeloSalon::buscarHoraPicoSalonModel($hora, $salon);
        return $mostrar;
    }

    public function mostrarDatosDetalleReservaControl()
    {
        $comando = ModeloSalon::comandoSQL();
        $mostrar = ModeloSalon::mostrarDatosDetalleReservaModel();
        return $mostrar;
    }

    public function mostrarDatosDetalleReservaUsuarioControl($id)
    {
        $comando = ModeloSalon::comandoSQL();
        $mostrar = ModeloSalon::mostrarDatosDetalleReservaUsuarioModel($id);
        return $mostrar;
    }

    public function mostrarDatosIdReservaControl($id)
    {
        $comando = ModeloSalon::comandoSQL();
        $mostrar = ModeloSalon::mostrarDatosIdReservaModel($id);
        return $mostrar;
    }

    public function mostrarDatosIdDetalleReservaControl($id)
    {
        $comando = ModeloSalon::comandoSQL();
        $mostrar = ModeloSalon::mostrarDatosIdDetalleReservaModel($id);
        return $mostrar;
    }

    public function buscarDatosDetalleReservaControl($datos)
    {

        $salon = ($datos['salon'] == '') ? '' : ' AND r.id_salon = ' . $datos['salon'];
        $fecha = ($datos['fecha'] == '') ? '' : ' AND r.fecha_reserva = "' . $datos['fecha'] . '"';

        $datos = array('salon' => $salon, 'fecha' => $fecha, 'user' => $datos['user']);

        $comando = ModeloSalon::comandoSQL();
        $mostrar = ModeloSalon::buscarDatosDetalleReservaModel($datos);
        return $mostrar;
    }

    public function buscarDatosDetalleReservaUsuarioControl($datos)
    {

        $salon = ($datos['salon'] == '') ? '' : ' AND r.id_salon = ' . $datos['salon'];
        $fecha = ($datos['fecha'] == '') ? '' : ' AND r.fecha_reserva = "' . $datos['fecha'] . '"';

        $datos = array('salon' => $salon, 'fecha' => $fecha, 'user' => $datos['user'], 'id' => $datos['id']);

        $comando = ModeloSalon::comandoSQL();
        $mostrar = ModeloSalon::buscarDatosDetalleReservaUsuarioModel($datos);
        return $mostrar;
    }

    public function guardarSalonControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'   => $_POST['id_log'],
                'nombre'   => $_POST['nombre'],
                'horas'    => ($_POST['horas'] * 2),
                'dias'     => $_POST['dias'],
                'cantidad' => $_POST['cantidad'],
            );

            $guardar = ModeloSalon::guardarSalonModel($datos);

            if ($guardar['guardar'] == true) {

                if (isset($_POST['horas_pico'])) {

                    $array_horas = array();
                    $array_horas = $_POST['horas_pico'];

                    $it = new MultipleIterator();

                    $it->attachIterator(new ArrayIterator($array_horas));

                    foreach ($it as $hora) {

                        $datos_horas = array(
                            'id_salon' => $guardar['id'],
                            'hora'     => $hora[0],
                            'id_log'   => $_POST['id_log'],
                        );

                        $guardar_hora = ModeloSalon::guardarHorasPicoModel($datos_horas);
                    }
                }

                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error al crear salon", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function editarSalonControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'id_log'   => $_POST['id_log'],
                'id_salon' => $_POST['id_salon'],
                'nombre'   => $_POST['nombre_edit'],
                'hora'     => ($_POST['hora_edit'] * 2),
                'dias'     => $_POST['dias_edit'],
                'cantidad' => $_POST['cantidad_edit'],
            );

            $editar = ModeloSalon::editarSalonModel($datos);

            if ($editar == true) {

                $desactivar_horas = ModeloSalon::desactivarHorasPicoSalonModel($datos);

                if (isset($_POST['horas_pico_edit']) && $desactivar_horas == true) {

                    $array_horas = array();
                    $array_horas = $_POST['horas_pico_edit'];

                    $it = new MultipleIterator();

                    $it->attachIterator(new ArrayIterator($array_horas));

                    foreach ($it as $hora) {

                        $datos_horas = array(
                            'id_salon' => $_POST['id_salon'],
                            'hora'     => $hora[0],
                            'id_log'   => $_POST['id_log'],
                        );

                        $guardar_hora = ModeloSalon::guardarHorasPicoModel($datos_horas);
                    }
                }

                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error al editar salon", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function reservarSalonControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $array_horas = array();
            $array_horas = $_POST['horas'];

            $it = new MultipleIterator();

            $it->attachIterator(new ArrayIterator($array_horas));

            $datos = array(
                'id_log'          => $_POST['id_log'],
                'apartamento'     => $_POST['apartamento'],
                'salon'           => $_POST['salon'],
                'fecha'           => $_POST['fecha_apartar'],
                'detalle_reserva' => $_POST['detalle_reserva'],
                'id_log'          => $_POST['id_log'],
            );

            $guardar = ModeloSalon::reservarSalonModel($datos);

            if ($guardar['guardar'] == true) {

                foreach ($it as $hora) {
                    $datos_hora = array(
                        'id_reserva'     => $guardar['id'],
                        'id_apartamento' => $_POST['apartamento'],
                        'id_salon'       => $_POST['salon'],
                        'horas'          => $hora[0],
                    );

                    $guardar_hora = ModeloSalon::reservarHorasModel($datos_hora);
                }

                if ($guardar_hora == true) {

                    $comando           = ModeloSalon::comandoSQL();
                    $datos_usuario     = ModeloUsuarios::mostrarDatosUsuariosModel($_POST['id_log']);
                    $datos_salon       = ModeloSalon::mostrarDatosSalonesModel($_POST['salon']);
                    $datos_apartamento = ModeloTorre::mostrarDatosApartamentoModel($_POST['apartamento']);

                    $horas_reservadas = ModeloSalon::mostrarHorasReservadasModel($guardar['id']);

                    $horas_texto = '';

                    foreach ($horas_reservadas as $horas_hoy) {
                        $horas_texto .= $horas_hoy['horas'] . ', ';
                    }

                    $mensaje = '
                    <div>
                    <p style="font-size: 1.3em;">
                    El usuario <b>' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '</b> ha reservado el salon:
                    </p>
                    <p style="font-size: 1.3em;">
                    <ul style="font-size: 1.3em;">
                    <li><b>Salon:</b> ' . $datos_salon['nombre'] . '</li>
                    <li><b>Apartamento:</b> ' . $datos_apartamento['nom_apar'] . '</li>
                    <li><b>Fecha reservado:</b> ' . $_POST['fecha_apartar'] . '</li>
                    <li><b>Horas reservadas:</b> ' . $horas_texto . '</li>
                    <li><b>Detalle de reserva:</b> ' . $_POST['detalle_reserva'] . '</li>
                    </ul>
                    </p>
                    </div>
                    ';

                    $datos_correo = array(
                        'asunto'  => 'Reserva de salon (' . $datos_salon['nombre'] . ')',
                        'correo'  => array('admirage57@gmail.com', 'angel.vargas@royalschool.edu.co', 'jesus.polo@royalschool.edu.co'),
                        'user'    => 'Administrador',
                        'mensaje' => $mensaje,
                    );

                    $enviar_correo = Correo::enviarCorreoModel($datos_correo);

                    echo '
                    <script>
                    ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina,1050);

                    function recargarPagina(){
                        window.location.replace("apartar");
                    }
                    </script>
                    ';
                } else {
                    echo '
                    <script>
                    ohSnap("Error al reservar salon", {color: "red"});
                    </script>
                    ';
                }
            }
        }
    }

    public function cancelarReservaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'             => $_POST['id_log'],
                'id_reserva'         => $_POST['id_reserva'],
                'motivo'             => $_POST['motivo'],
                'fecha'              => date('Y-m-d H:i:s'),
                'id_detalle_reserva' => $_POST['id_detalle_reserva'],
            );

            $cancelar = ModeloSalon::cancelarReservaModel($datos);

            if ($cancelar == true) {

                $comando               = ModeloSalon::comandoSQL();
                $datos_usuario         = ModeloUsuarios::mostrarDatosUsuariosModel($_POST['id_log']);
                $datos_reserva         = ModeloSalon::mostrarDatosIdReservaModel($_POST['id_reserva']);
                $datos_salon           = ModeloSalon::mostrarDatosSalonesModel($datos_reserva['id_salon']);
                $datos_apartamento     = ModeloTorre::mostrarDatosApartamentoModel($datos_reserva['id_apartamento']);
                $datos_detalle_reserva = ModeloSalon::mostrarDatosIdDetalleReservaModel($_POST['id_detalle_reserva']);

                $mensaje = '
                <div>
                <p style="font-size: 1.3em;">
                El usuario <b>' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '</b> ha cancelado la reserva del salon:
                </p>
                <p style="font-size: 1.3em;">
                <ul style="font-size: 1.3em;">
                <li><b>Salon:</b> ' . $datos_salon['nombre'] . '</li>
                <li><b>Apartamento:</b> ' . $datos_apartamento['nom_apar'] . '</li>
                <li><b>Fecha reservado:</b> ' . $datos_reserva['fecha_reserva'] . '</li>
                <li><b>Horas cancelada:</b> ' . $datos_detalle_reserva['horas'] . '</li>
                <li><b>Fecha de cancelacion:</b> ' . $datos_detalle_reserva['fecha_cancelacion'] . '</li>
                <li><b>Motivo de cancelacion:</b> ' . $datos_detalle_reserva['motivo_cancelacion'] . '</li>
                </ul>
                </p>
                </div>
                ';

                $datos_correo = array(
                    'asunto'  => 'Cancelacion de reserva del salon (' . $datos_salon['nombre'] . ')',
                    'correo'  => array('admirage57@gmail.com', 'angel.vargas@royalschool.edu.co', 'jesus.polo@royalschool.edu.co'),
                    'user'    => 'Administrador',
                    'mensaje' => $mensaje,
                );

                $enviar_correo = Correo::enviarCorreoModel($datos_correo);

                echo '
                <script>
                ohSnap("Cancelado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("reservas");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error al cancelar reserva", {color: "red"});
                </script>
                ';
            }

        }
    }

    public function mostrarDatosSalonIdControl($id)
    {
        $comando = ModeloSalon::comandoSQL();
        $mostrar = ModeloSalon::mostrarDatosSalonIdModel($id);
        return $mostrar;
    }

    public function mostrarHorasDisponiblesControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['salon']) &&
            !empty($_POST['salon'])
        ) {

            $datos = array(
                'apartamento' => $_POST['apartamento'],
                'salon'       => $_POST['salon'],
                'fecha'       => $_POST['fecha'],
            );

            $comando        = ModeloSalon::comandoSQL();
            $datos_salon    = ModeloSalon::mostrarDatosSalonIdModel($_POST['salon']);
            $ultima_reserva = ModeloSalon::ultimaReservaSalonModel($datos);

            $fecha_anterior = date("Y-m-d", strtotime($_POST['fecha'] . "- 1 days"));
            $fecha_proxima  = date("Y-m-d", strtotime($ultima_reserva['fecha_reserva'] . "+ " . $datos_salon['dias'] . " days"));

            $consultar_reserva = ModeloSalon::reservaSalonFechaAnteriorModel($datos, $fecha_anterior);
            $consultar_reserva = ($consultar_reserva['id'] == '') ? ModeloSalon::reservaSalonFechaAnteriorModel($datos, $ultima_reserva['fecha_reserva']) : $consultar_reserva;
            $consultar_reserva = ($consultar_reserva['id'] == '') ? ModeloSalon::reservaSalonFechaAnteriorModel($datos, $fecha_proxima) : $consultar_reserva;

            if ($consultar_reserva['id'] != '' && $_POST['fecha'] == $fecha_proxima && $datos_salon['hora_pico'] != '') {
                $mostrar = ModeloSalon::noMostrarHorasPicoDisponiblesModel($datos);
            } else {
                $mostrar = ModeloSalon::mostrarHorasDisponiblesModel($datos);
            }

            return $mostrar;
        }
    }

    public function validarSemanaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['apartamento']) &&
            !empty($_POST['apartamento'])
        ) {

            $inicio_semana = date("Y-m-d", strtotime('monday this week', strtotime($_POST['fecha'])));
            $fin_semana    = date("Y-m-d", strtotime('sunday this week', strtotime($_POST['fecha'])));

            $salon       = (!empty($_POST['salon'])) ? $_POST['salon'] : 0;
            $apartamento = (!empty($_POST['apartamento'])) ? $_POST['apartamento'] : 0;

            $datos = array(
                'apartamento'   => $apartamento,
                'salon'         => $salon,
                'fecha'         => $_POST['fecha'],
                'inicio_semana' => $inicio_semana,
                'fin_semana'    => $fin_semana,
            );

            $mensaje = 'vacio';

            $comando        = ModeloSalon::comandoSQL();
            $semana         = ModeloSalon::validarSemanaModel($datos);
            $datos_salon    = ModeloSalon::mostrarDatosSalonIdModel($_POST['salon']);
            $ultima_reserva = ModeloSalon::ultimaReservaSalonModel($datos);

            if ($semana != false) {
                if ($semana['cantidad_reservado'] == $datos_salon['cantidad']) {
                    $mensaje = 'semana';
                } else {

                    $datos_hora = array(
                        'fecha'      => $_POST['fecha'],
                        'id_reserva' => $semana['id'],
                    );

                    $horas_dias = ModeloSalon::validarHorasModel($datos_hora);

                    if ($horas_dias['cantidad_horas'] == $datos_salon['horas']) {
                        $mensaje = 'horas';
                    }

                    $fecha_proxima = (!empty($ultima_reserva['fecha_reserva'])) ? date("Y-m-d", strtotime($ultima_reserva['fecha_reserva'] . "+ " . $datos_salon['dias'] . " days")) : date('Y-m-d');

                    if ($datos_salon['dias'] > 1) {
                        if ($_POST['fecha'] < $fecha_proxima && $_POST['fecha'] != date('Y-m-d')) {
                            $mensaje = 'fecha';
                        }
                    } else {
                        $mensaje = 'vacio';
                    }

                }
            }

            return $mensaje;
        }
    }

    public function horasRestantesSalonControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['apartamento']) &&
            !empty($_POST['apartamento'])
        ) {
            $datos = array(
                'apartamento' => $_POST['apartamento'],
                'salon'       => $_POST['salon'],
                'fecha'       => $_POST['fecha'],
            );

            $mostrar = ModeloSalon::horasRestantesSalonModel($datos);
            $horas   = ($mostrar['horas_restantes'] == "") ? $mostrar['horas'] : $mostrar['horas_restantes'];
            return $horas;
        }
    }
}
