<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'parametros' . DS . 'ModeloParametros.php';
require_once CONTROL_PATH . 'hash.php';

class ControlParametros
{

    private static $instancia;

    public static function singleton_parametros()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarAniosControl()
    {
        $comando = ModeloParametros::comandoSQL();
        $mostrar = ModeloParametros::mostrarAniosModel();
        return $mostrar;
    }

    public function mostrarDatosEmpresaControl()
    {
        $comando = ModeloParametros::comandoSQL();
        $mostrar = ModeloParametros::mostrarDatosEmpresaModel();
        return $mostrar;
    }

    public function mostrarDatosParametrosControl()
    {
        $comando = ModeloParametros::comandoSQL();
        $mostrar = ModeloParametros::mostrarDatosParametrosModel();
        return $mostrar;
    }

    public function mostrarDatosParametrosAnioControl($anio)
    {
        $comando = ModeloParametros::comandoSQL();
        $mostrar = ModeloParametros::mostrarDatosParametrosAnioModel($anio);
        return $mostrar;
    }

    public function mostrarDatosAnioControl($id)
    {
        $comando = ModeloParametros::comandoSQL();
        $mostrar = ModeloParametros::mostrarDatosAnioModel($id);
        return $mostrar;
    }

    public function mostrarDatosMesesAnioControl($id)
    {
        $comando = ModeloParametros::comandoSQL();
        $mostrar = ModeloParametros::mostrarDatosMesesAnioModel($id);
        return $mostrar;
    }

    public function actualizarInformacionEmpresaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'id_log'     => $_POST['id_log'],
                'id_empresa' => $_POST['id_empresa'],
                'nombre'     => $_POST['nombre'],
                'nit'        => $_POST['nit'],
                'telefono'   => $_POST['telefono'],
                'direccion'  => $_POST['direccion'],
            );

            $actualizar = ModeloParametros::actualizarInformacionEmpresaModel($datos);

            if ($actualizar == true) {
                echo '
                <script>
                ohSnap("Actualizado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function agregarValoresControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'      => $_POST['id_log'],
                'valor_admin' => str_replace(',', '', $_POST['valor_admin']),
                'dia_inicio'  => $_POST['dia_inicio'],
                'dia_fin'     => $_POST['dia_fin'],
                'anio'        => $_POST['anio'],
            );

            $guardar = ModeloParametros::agregarValoresModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function agregarAnioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'id_log'     => $_POST['id_log'],
                'anio'       => $_POST['anio'],
                'valor_tasa' => $_POST['valor_tasa'],
            );

            $guardar = ModeloParametros::agregarAnioModel($datos);

            if ($guardar['guardar'] == true) {

                for ($i = 1; $i < 13; $i++) {

                    $valor_tasa = (date('m') == $i) ? $_POST['valor_tasa'] : '0';

                    $datos_mes = array(
                        'id_log'     => $_POST['id_log'],
                        'anio'       => $guardar['id'],
                        'mes'        => $i,
                        'valor_tasa' => $valor_tasa,
                    );

                    $guardar_mes = ModeloParametros::agregarMesesAnioModel($datos_mes);

                }

                if ($guardar_mes == true) {

                    echo '
                    <script>
                    ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina,1050);

                    function recargarPagina(){
                        window.location.replace("index");
                    }
                    </script>
                    ';
                }
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function actualizarValoresMesAnioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'id_log'      => $_POST['id_log'],
                'valor'       => $_POST['valor_tasa_edit'],
                'id_mes_anio' => $_POST['id_mes_anio'],
            );

            $guardar = ModeloParametros::actualizarValoresMesAnioModel($datos);

            if ($guardar == true) {

                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("detalle&anio=' . base64_encode($_POST['id_anio']) . '");
                }
                </script>
                ';
            }
        } else {
            echo '
            <script>
            ohSnap("Ha ocurrido un error!", {color: "red"});
            </script>
            ';
        }
    }

    public function activarAnioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $activar = ModeloParametros::activarAnioModel($_POST['id']);
            $mensaje = ($activar == true) ? 'ok' : 'no';
            return $mensaje;
        }
    }

    public function inactivarAnioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $activar = ModeloParametros::inactivarAnioModel($_POST['id']);
            $mensaje = ($activar == true) ? 'ok' : 'no';
            return $mensaje;
        }
    }
}
